#include"meme.hpp"

//Parser-----------------------------------------------------------------------
uint32_t Parser::parseSeqs(const std::string& seqFile)
{
    std::string line;
    std::ifstream f (seqFile, std::ifstream::in);

    if(f.is_open())
    {
        while(getline(f, line)) seqs_.append(line);
    }
    f.close();
    return line.length();
}

uint32_t Parser::parseProfile(const std::string& profileFile)
{
    std::string  line;
    std::ifstream fp (profileFile, std::ifstream::in);
    
    if(fp.is_open())
    {
        while(getline(fp, line)) genNum(line);
    }
    fp.close();
    return line.length();
}

void Parser::genNum(const std::string& line)
{
    std::string tmp_str;
    float tmp_int;
    std::stringstream ss; 
    ss << line;                                     //convert string into stream object
    
    while(!ss.eof())
    { 
        ss >> tmp_str;                              //take words into tmp_str one by one
        //try to convert string to int
        if(std::stringstream(tmp_str) >> tmp_int) profile_.push_back(tmp_int);

        tmp_str.clear(); 
    }
}

std::string Parser::getSeq() const {
    return seqs_;
}

std::vector<float> Parser::getProfile() const{
    return profile_;
}

//Meme-------------------------------------------------------------------------
Meme::Meme(const std::vector<float> profileMatrix, const std::string& seqsMatrix, 
           const uint32_t its, const uint32_t seqLength, const u_int32_t profileLength)
:seqs_(seqsMatrix), iterats_(its), seqLength_(seqLength), proLength_(profileLength)
{
    pMatrix_ = profileMatrix;
}
