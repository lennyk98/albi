#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>

class Parser
{
    public:
        /**
        * @brief parses the seqs.txt file to an string
        * @param seqsFile the name of the file containing the sequences
        * @return length of one sequence (all have the same!)
        */
        uint32_t parseSeqs(const std::string& seqsFile);
        
        /**
        * @brief parses the profile.txt file to an vector of floats
        * @param profileFILE the name of the file containing the sequences
        * @return length of one profileMatrix row (all have the same!)
        */  
        uint32_t parseProfile(const std::string& profileFile);

        /**
        * @brief extracts numbers of string 
        * @param line one line out of our profileMatrix
        */
        void genNum(const std::string& line);

        std::string getSeq() const;

        std::vector<float> getProfile() const;
    private:
        std::string seqs_;
        std::vector<float> profile_;
};

class Meme
{
    public:
        /**
        * @brief constructs or meme object with init-list
        * @param profileMatrix our pMatrix(we change it later) and build up wMatrix
        * @param seqsBlock block of different sequences, its constant will be used
        * @param its number of iterations
        */
        Meme(const std::vector<float> profileMatrix, const std::string& seqBlock, 
             const uint32_t its, const uint32_t seqLength, const u_int32_t profileLength);        
        
        /**
        * @brief return current profileMatrix
        * @return current pMatrix
        */
        std::vector<float> get_Profile();

        /**
        * @brief returns consensus motif
        * @return string composed of N.A. with most likelyhood 
        */
        std::string get_Consensus();

        /**
        * @brief computes the wMatrix 
        */
        void compute_wMatrix();

        /**
        * @brief computes pMatrix
        */
        void compute_pMatrix();

    private:
        std::vector<float> pMatrix_ {};
        std::vector<float> wMatrix_ {}; 

        const std::string& seqs_;       //all sequences -> they have to be the same length!
        const uint32_t seqLength_;      //stores length of sequence
        const uint32_t proLength_;
        const uint32_t iterats_;        //stores number of iterations
};