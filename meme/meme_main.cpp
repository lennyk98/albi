#include "meme.hpp"

int main(int argc, const char* argv[])
{
    if(argc == 4)
    {
        //do meme
        Parser p;
        p.parseSeqs(argv[1]);
        p.parseProfile(argv[2]);
        auto x = p.getProfile();
        for(const auto e : x) std::cout << e << '\n';
        std::cout << "BLA\n";
        return 0;
    }
    else return 1;
}