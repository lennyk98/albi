import csv
import random
import sys
import math
import matplotlib.pyplot as plt
import numpy as np

ALPHABET = {'A':0.3, 'C':0.2, 'G':0.2, 'T':0.3}
MATRIX = {'A' : [],'C' : [],'G' : [],'T' : []}
SUM = 0
KONSENSUS = ['A','C','A','G','A','T','A','A','G','A','A']

def readMatrix():
    mat = []
    global SUM
    with open(sys.argv[1], newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            mat.append(row)
    
    for i in range(1,len(mat)):
        for val in mat[i]:
            if val == 'A':
                MATRIX['A'] = list(map(int, mat[i][1:len(mat[i])]))
            if val == 'C': 
                MATRIX['C'] = list(map(int, mat[i][1:len(mat[i])]))
            if val == 'G':
                MATRIX['G'] = list(map(int, mat[i][1:len(mat[i])]))
            else: 
                MATRIX['T'] = list(map(int, mat[i][1:len(mat[i])]))

    SUM = MATRIX['A'][0] + MATRIX['C'][0] + MATRIX['G'][0] + MATRIX['T'][0]                         

def genSeq():
        return (random.choices(list(ALPHABET.keys()), weights= list(ALPHABET.values()), k=200))

def compute(vals, key):
    sol = []
    for i in vals:
        sol.append(round(math.log2(((i+1)/(SUM+4))/ALPHABET[key])))
    return sol

def computePSSM():
    newMAT = {}
    for k,v in MATRIX.items():
        newMAT[k] = compute(v,k)
    return newMAT

def findScore(matrix, seq):
    score = 0
    i = 0
    for c in seq:
        score += matrix[c][i]
        i += 1
    return score

def generateScores(matrix, seq):
    scores = []
    for i in range(0, len(seq)-11):
        tmp_seq = seq[i:i+11]
        scores.append(findScore(matrix, tmp_seq))
    return scores

def scoreStuff(score):
    #make scores positive
    for i in range(len(score)):
        score[i] = 2**(score[i])
        
    s = sum(score)
    #normalize -> sum(score) == 1
    for i in range(len(score)):
        score[i] /= s

    return score


if __name__ == '__main__':
    readMatrix()
    
    SEQ = genSeq()
    MAT = computePSSM()
    SCO_0 = scoreStuff(generateScores(MAT, SEQ))

    #100 mal ziehen & zählen 
    v, c = (np.unique(np.random.choice(np.arange(len(SCO_0)), 100, p = SCO_0, replace=True), return_counts=True))
    print("Random-Vals & Counts:")
    print(v, c)

    #integrate konsensus-------------------------------------------------------
    SEQ = KONSENSUS + SEQ[0:len(SEQ)-len(KONSENSUS)]
    SCO_1 = scoreStuff(generateScores(MAT, SEQ))

    #100 mal ziehen & zählen 
    v, c = (np.unique(np.random.choice(np.arange(len(SCO_1)), 100, p = SCO_1, replace=True), return_counts=True))
    print("With consensus motif:")
    print(v, c)


    plt.figure(1)
    plt.plot(SCO_0, label = "Random")
    plt.plot(SCO_1, label = "Konsensus")
    plt.ylabel("Prob")
    plt.xlabel("Index")
    plt.legend()
    plt.savefig("xxx.png")



