#include "upgma.hpp"
#include "parser.hpp"
#include <iostream>

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        std::cout << "One Argument needed!! Scoring Matrix \n";
        return -1;
    }

    //create parser object to handle data_inout & conversion
    Parser p;
    auto matrix = p.parseMatrix_(argv[1]);

    //create UPGMA
    Upgma u;
    u.setMatrix(matrix);
    std::cout << "GIVEN MATRIX \n";
    for(auto const &  e : u.getMatrix()) e.print();
    std::cout << "FIRST ROTATION \n\n\n";
    u.findMini();
    auto x = u.joinCluster();
    u.computeDist(x);
    for(auto const &  e : u.getMatrix()) e.print();
    
    std::cout << "SECOND ROTATION \n\n\n";
    u.findMini();
    auto y = u.joinCluster();
    u.computeDist(y);
    for(auto const &  e : u.getMatrix()) e.print();

    std::cout << "THIRD ROTATION \n\n\n";
    u.findMini();
    auto z = u.joinCluster();
    u.computeDist(z);
    for(auto const &  e : u.getMatrix()) e.print();

    std::cout << "FOURTH ROTATION \n\n\n";
    u.findMini();
    auto zz = u.joinCluster();
    u.computeDist(zz);
    for(auto const &  e : u.getMatrix()) e.print();

    
    return 0;
}