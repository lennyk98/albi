#include "parser.hpp"
#include "upgma.hpp"

std::vector<float> Parser::getNum_(std::string const& s)
{
    std::vector<float> nums {};
    std::string tmp_str;
    float tmp_int;
    std::stringstream ss; 
    ss << s;                                        //convert string into string stream
    
    while(!ss.eof())
    { 
        ss >> tmp_str;                              //take words into tmp_str one by one
        if(std::stringstream(tmp_str) >> tmp_int)   //try to convert string to int
        {   
            nums.push_back(tmp_int);
        }
        tmp_str.clear();
    }
    return nums;
}

std::vector<Cluster> Parser::parseMatrix_(std::string const& file_name)
{
    std::string line;
    std::vector<std::string> data;
    std::vector<Cluster> mat;
    std::fstream fs (file_name, std::fstream::in);
    
    if (fs.is_open())
    {
        while(getline(fs, line))
        {
            data.push_back(line);
        }
         fs.close();
    }

    for(uint32_t i = 0; i < data.size(); ++i)
    {
        //fill in cluster
        if(i == 0)
        {
            for(uint32_t j = 0; j < data[i].size(); ++j)
            {
                //check in first line all characters -> look ascii table
                //init all given clusters & check for valid names!!
                if((int(data[i][j]) >= 65 && int(data[i][j]) <= 90)|| (int(data[i][j]) >= 97 && int(data[i][j]) <= 122))
                {
                    Cluster c;
                    c.name = data[i][j];
                    c.count = 1;
                    mat.push_back(c);
                }
            }  
        }
        //fill in distances from each cluster to each cluster
        mat[i].dist = getNum_(data[i+1]);
        //fill in neighbors
        for(auto const & e : mat) mat[i].neighbors.push_back(e.name);
    }
    return mat;
}