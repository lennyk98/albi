#pragma once

#include "upgma.hpp"
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

//small object to read in matrix & convert it into clusters
class Parser 
{   
    public:
        /**
        * @brief extracts all numbers out of an string 
        * @param s the string to search through.
        * @return vector with all numbers out of @p s
        */
        std::vector<float> getNum_(std::string const& s);
        
        /**
        * @brief reads txt file line by line as strings & passes them to getNum_ 
        * @param file_name name of the file 
        */
        std::vector<Cluster> parseMatrix_(std::string const& file_name);
};