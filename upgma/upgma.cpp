#include "upgma.hpp"

//Simple Cluster print function
void Cluster::print() const
{
    std::cout << "Name : " << this -> name << "     count :" <<  this -> count << '\n';
    std::cout << "Neighbors:            Distance: \n";
    for(uint32_t i = 0; i < this -> neighbors.size(); ++i)
    {
        std::cout << this -> neighbors[i] << " : " << this -> dist[i] << '\n';
    }
    std::cout << '\n';
}

// Getter & Setter ------------------------------------------------------------
const std::vector<Cluster>& Upgma::getMatrix() const
{
    return this -> matrix_;
}

const std::tuple<uint32_t, uint32_t>& Upgma::getMini() const
{
    return this -> mini_;
}

void Upgma::setMatrix(const std::vector<Cluster>& matrix)
{
    this -> matrix_ = matrix;
}

void Upgma::setMini(const std::tuple<uint32_t, uint32_t>& mini)
{
    this -> mini_ = mini;
}

// Where the actual stuff happens ---------------------------------------------
//Step 1 
void Upgma::findMini()
{
    //tmp minimum to find min out of all distances in matrix
    float min_tmp = 1000;
    
    //iterate through each cluster.distance in matrix
    for(uint32_t i = 0; i < this -> matrix_.size(); ++i)
    {
        for(uint32_t j = 0; j < this -> matrix_[i].dist.size(); ++j)
        {
            if(min_tmp > this -> matrix_[i].dist[j] && this -> matrix_[i].dist[j] > 0)
            {
                min_tmp = this -> matrix_[i].dist[j];
                this -> mini_ = {i, j};
            }
        }
    }
}

//Step 2 join cluster
Cluster Upgma::joinCluster()
{
    //create new cluster & join cluster with min distance togther
    const auto[i, j] = this -> mini_;
    Cluster u = this -> matrix_[i];
    u.count += this -> matrix_[j].count;
    u.name.append(this -> matrix_[j].neighbors[j]);
    u.dist[j] = 0;

    //update distance matrix with new cluster
    return u;
}

//Step 2.1 recompute distanced for new cluster
void Upgma::computeDist(Cluster& u)
{
    //get old cluster
    const auto[a, b] = this -> mini_;
    Cluster c0 = this -> matrix_[a];
    Cluster c1 = this -> matrix_[b];
    //delete old neigbors & distances
    u.neighbors.clear();
    u.dist.clear();
    //has to be neighbor to itself for indices to work & 
    u.dist.insert(u.dist.begin(), 0);
    u.neighbors.push_back(u.name);

    //compute new distances
    for(uint32_t i = 0; i < this -> matrix_[b].dist.size(); ++i)
    {
        if(this -> matrix_[i].name != c0.name && this -> matrix_[i].name != c1.name)
        {
            //std::cout << c0.count << '*' <<  c0.dist[i] << '+' << c1.count << '*' <<  c1.dist[i] << '/' << (c0.count + c1.count) << '\n';
            float x = (c0.count * c0.dist[i] + c1.count * c1.dist[i]) / (c0.count + c1.count);
            u.dist.push_back(x);
            u.neighbors.push_back(c0.neighbors[i]);
        }
    }
    //erase old entries
    this -> matrix_.erase(this -> matrix_.begin() + a);
    this -> matrix_.erase(this -> matrix_.begin() + b-1);

    updateMatrix(u);
}

//Step 3 compute new distances & update matrix
void Upgma::updateMatrix(const Cluster& u)
{
    std::vector<Cluster> newMatrix{};
    const auto[a, b] = this -> mini_;
    newMatrix.push_back(u);
    for(uint32_t i = 0; i < this -> matrix_.size(); ++i)
    {
        auto c = this -> matrix_[i];
        //erase old neighbors from older clusters
        c.neighbors.erase(c.neighbors.begin() + a);
        c.neighbors.erase(c.neighbors.begin() + b-1);
        //erase old distances from older clusters
        c.dist.erase(c.dist.begin() + a);
        c.dist.erase(c.dist.begin() + b-1);

        newMatrix.push_back(c);
    }
    //every other cluster needs distance too
    for(uint32_t i = 1; i < u.neighbors.size(); ++i)
    {
            newMatrix[i].neighbors.insert(newMatrix[i].neighbors.begin(), u.name);
            newMatrix[i].dist.insert(newMatrix[i].dist.begin(), u.dist[i]);
    }
        
    this -> matrix_ = newMatrix;
}