#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <tuple>
#include <math.h>

struct Cluster
{
    std::string name;
    std::vector<std::string> neighbors;
    std::vector<float> dist {};
    uint32_t count = 0;
    
    void print() const;
};


class Upgma
{
    private:
        std::vector<Cluster> matrix_ {};
        std::tuple<uint32_t, uint32_t> mini_ {};
        bool singleLinkage = false;

    public:
        //Getter
        //---------------------------------------------------------------------
        const std::vector<Cluster>& getMatrix() const;

        const std::tuple<uint32_t, uint32_t>& getMini() const;

        //Setter
        //---------------------------------------------------------------------
        void setMatrix(const std::vector<Cluster>& matrix);

        void setMini(const std::tuple<uint32_t, uint32_t>& mini);

        //Compute
        //---------------------------------------------------------------------
        /**
        * @brief finds minimum in matrix and saves position in member var
        */
        void findMini();

        /**
        * @brief joins two cluster
        * @return new cluster
        */
        Cluster joinCluster();

        /**
        * @brief recomputes distances depending on member singleLinkage
        * @return distance to each cluster
        */
        void computeDist(Cluster& u);

        /**
        * @brief updates matrix object with new distances
        * @return new matrix
        */
        void updateMatrix(const Cluster& u);


        //Newick
        /**
        * @brief builds NewickFormat string from last cluster .
        * @return the Newick-String
        */
        const std::string& toNewick() const;

        //Print
        void print() const;
};